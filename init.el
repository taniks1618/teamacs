;; Disabling the startpage
;; (setq inhibit-startup-message t)    

;; Disables visible scrollbar
(scroll-bar-mode -1)    

;; Disables the toolbar
(tool-bar-mode -1)      

;; Disables tooltips
(tooltip-mode -1)       

;; Gives 
(set-fringe-mode 10)    

;; Disables the menubar
(menu-bar-mode -1)      

;; Disables the bell
(setq visible-bell t)

; Line numbers
(column-number-mode)
(global-display-line-numbers-mode t)
;; Adds a hook to disable line #'s in these mode
(dolist (mode   '(org-mode-hook
                 term-mode-hook
                 treemacs-mode-hook
                 vterm-mode-hook
                 eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; Default Font
(set-face-attribute 'default nil :font "Iosevka Nerd Font Mono" :height 120)
;; The fixed pitch font
(set-face-attribute 'fixed-pitch nil :font "Iosevka Nerd Font Mono" :height 140)

;; Stes the variable pitch font
(set-face-attribute 'variable-pitch nil :font "Iosevka Nerd Font Mono" :height 165 :weight 'regular)

(set-frame-parameter (selected-frame) 'alpha '(90 . 90))
(add-to-list 'default-frame-alist '(alpha . (90 . 90)))
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; Gets escape to kill a window/buffer/dialog box.
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(require 'package)

;; All of the package archives I use I could also use MELPA STABLE
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

;; For the non-linux
(unless (package-installed-p 'use-package)
            (package-install 'use-package))


(require 'use-package)
(setq use-package-always-ensure t)

;; Autocomplete engine for M-x and others
(use-package ivy
    :diminish
    :bind (("C-s" . swiper)
        :map ivy-minibuffer-map
        ("TAB" . ivy-alt-done)	
        ("C-l" . ivy-alt-done)
        ("C-j" . ivy-next-line)
        ("C-k" . ivy-previous-line)
        :map ivy-switch-buffer-map
        ("C-k" . ivy-previous-line)
        ("C-l" . ivy-done)
        ("C-d" . ivy-switch-buffer-kill)
        :map ivy-reverse-i-search-map
        ("C-k" . ivy-previous-line)
        ("C-d" . ivy-reverse-i-search-kill))
    :config
    (ivy-mode 0))

;; Adds a description to the M-x menu.
(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-ibuffer)
         ("M-f" . counsel-find-file)
         ("C-M-l" . counsel-imenu)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-config))
  :config
;; history it so that searches don't start with ^
  (setq ivy-initial-inputs-alist nil))

;; For icons on Doom Modeline
(use-package all-the-icons)

;; For the bar at the bottom from Doom Emacs
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

;; For themes like Gruvbox, Nord, One Dark and many others curtiosy of Doom Emacs!
(use-package doom-themes
    ;; Nord theme
    :init (load-theme 'doom-ephemeral t))

; Parenthesis reminder
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; Buffer in side to  see keystrokes
(use-package which-key
    :init (which-key-mode)
    :diminish which-key-mode
    :config
    (setq which-key-idle-delay 0))

;; Helpful Help
(use-package helpful
  :ensure t
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

; EVIL
(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

; General-el key binding package
(use-package general
    :config
    (general-create-definer tea/leader-keys
        :keymaps '(normal insert visual emacs)
        :prefix "SPC"
        :global-prefix "C-SPC"))

; Make keybinding with the CTRL-SPACE prefic
    (tea/leader-keys
        't'     '(:ignore t :which-key "toggles")
        'tt'    '(counsel-load-theme :which-key "choose theme")
        'RET'   '(vterm "vterm"))

(general-define-key
  "C-M-j" 'counsel-switch-buffer)

;; Hydra
(defhydra hydra-text-scale (:timeout 5)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

(tea/leader-keys
  "ts" '(hydra-text-scale/body :which-key "scale text"))

(defun tea/org-mode-setup ()
  (org-indent-mode)
  (variable-pitch-mode 1)
  (auto-fill-mode 0)
  (visual-line-mode 1)
  (setq evil-auto-indent nil))

(defun tea/org-font-setup ()
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•")))))))

;; Set faces for heading levels
(require 'org-faces)
(dolist (face '((org-level-1 . 1.2)
                (org-level-2 . 1.1)
                (org-level-3 . 1.05)
                (org-level-4 . 1.0)
                (org-level-5 . 1.1)
                (org-level-6 . 1.1)
                (org-level-7 . 1.1)
                (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :font "Cantarell" :weight 'regular :height (cdr face)))

;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)

(use-package org
  :hook (org-mode . tea/org-mode-setup)
  :config
  (setq org-ellipsis " ▾")
  (tea/org-font-setup))

  (setq org-agenda-start-with-log-mode t)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun tea/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . tea/org-mode-visual-fill))

;; Org-Babel
(org-babel-do-load-languages
  'org-babel-load-languages
  '((emacs-lisp . t)
    (python . t)
    (C . t)))

(setq org-confirm-babel-evaluate nil)

(require 'org-tempo)

(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("cl" . "src C"))

(setq org-agenda-files '("~/dox/org"))

(setq org-agenda-start-with-log-mode t)
(setq org-log-done 'time)
(setq org-log-into-drawer t)

;; For the lines in the startpage
(use-package page-break-lines)

;; Getting the actual package
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))

;; Title for startup
(setq dashboard-banner-logo-title "Welcome to Teamacs!")

;; Set the banner
;;(setq dashboard-startup-banner 'official)
(setq dashboard-startup-banner 'logo)

;; Center all of it!
(setq dashboard-center-content t)

;; The line breaks enable
(turn-on-page-break-lines-mode)

;; Icons
(setq dashboard-set-heading-icons t)
(setq dashboard-set-file-icons t)

;; Disableing Random footnote
(setq dashboard-set-footer nil)

;; All of Items in page
(setq dashboard-items '((recents . 10)
                        (agenda . 5)))

(use-package lsp-mode
        :commands (lsp lsp-deferred)
;;        :hook (lsp-mode . tea
        :init
        (setq lsp-keymap-prefix "C-c l")
        :config
        (lsp-enable-which-key-integration t))

(use-package lsp-ui
        :hook (lsp-mode . lsp-ui-mode)
        :custom
        (lsp-ui-doc-position 'bottom))
        :init (lsp-ui-mode 1)

(use-package lsp-treemacs
        :after lsp)

(use-package lsp-ivy)

(use-package flycheck)

(use-package lsp-pyright
        :ensure t
        :hook (python-mode . (lambda ()
                                (require 'lsp-pyright)
                                (lsp))))

(use-package company
  :after lsp-mode
  :hook (lsp-mode . company-mode)
  :bind (:map company-active-map
         ("<tab>" . company-complete-selection))
        (:map lsp-mode-map
         ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))
  :init (global-company-mode 1)

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  ;; NOTE: Set this to the folder where you keep your Git repos!
  (when (file-directory-p "~/")
    (setq projectile-project-search-path '("~/")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package magit
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))
;;(use-package evil-magit)
;;  :after magit)

;;(use-package term
;;  :config
;;  (setq explicit-shell-file-name "zsh") ;; Change this to zsh, etc
;;(setq explicit-zsh-args '()) Use 'explicit-<shell>-args for shell specific args

;;  (setq term-prompt-regexp "^[^#$%>\n]*[#$%>] *"))

;;(use-package eterm-256color
;;  :hook (term-mode . eterm-256color-mode))

(use-package vterm
        :commands vterm
        :config
        (setq term-prompt-regexp "^[^#$%>\n]*[#$%>] *")
        (setq vterm-shell "zsh")
        (setq vterm-max-scrollback 1000))

(defun tea/configure-eshell ()
  ;; Save command history when commands are entered
  (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)

  ;; Truncate buffer for performance
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)

  ;; Bind some useful keys for evil-mode
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "C-r") 'counsel-esh-history)
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "<home>") 'eshell-bol)
  (evil-normalize-keymaps)

  (setq eshell-history-size         1000
        eshell-buffer-maximum-lines 1000
        eshell-hist-ignoredups t
        eshell-scroll-to-bottom-on-input t))

(use-package eshell-git-prompt)

(use-package eshell
  :hook (eshell-first-time-mode . tea/configure-eshell)
  :config

  (with-eval-after-load 'esh-opt
    (setq eshell-destroy-buffer-when-process-dies t)
    (setq eshell-visual-commands '("htop" "zsh" "nvim" "ncmpcpp" "neomutt")))

  (eshell-git-prompt-use-theme 'simple))



;; Uncomment this for verbose package load up
;;(setq use-package-verbose t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(evil-magit magit which-key use-package rainbow-delimiters nord-theme ivy-rich hydra helpful general evil-visual-mark-mode evil-collection doom-themes doom-modeline counsel-projectile command-log-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
