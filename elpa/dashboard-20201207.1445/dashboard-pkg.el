(define-package "dashboard" "20201207.1445" "A startup screen extracted from Spacemacs"
  '((emacs "25.3")
    (page-break-lines "0.11"))
  :commit "f0387180a4d1540bb29562d65b141967339e7a18" :keywords
  ("startup" "screen" "tools" "dashboard")
  :authors
  (("Rakan Al-Hneiti"))
  :maintainer
  ("Rakan Al-Hneiti")
  :url "https://github.com/emacs-dashboard/emacs-dashboard")
;; Local Variables:
;; no-byte-compile: t
;; End:
