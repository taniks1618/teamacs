(define-package "vterm" "20201206.205" "Fully-featured terminal emulator"
  '((emacs "25.1"))
  :commit "a670b786539d3c8865d8f68fe0c67a2d4afbf1aa" :keywords
  ("terminals")
  :authors
  (("Lukas Fürmetz" . "fuermetz@mailbox.org"))
  :maintainer
  ("Lukas Fürmetz" . "fuermetz@mailbox.org")
  :url "https://github.com/akermu/emacs-libvterm")
;; Local Variables:
;; no-byte-compile: t
;; End:
