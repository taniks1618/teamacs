; UI
; Get rid of startpage
(setq inhibit-startup-message t)    

; Disables visible scrollbar
(scroll-bar-mode -1)    

; Disables the toolbar
(tool-bar-mode -1)      

; Disables tooltip
(tooltip-mode -1)       

; Gives room on the edge
(set-fringe-mode 10)    

; Disables the menubar
(menu-bar-mode -1)      

; Disables the bell
(setq visible-bell t)   

; Line numbers
(column-number-mode)
(global-display-line-numbers-mode t)

; Disables line numbers for ORG,TERM,ESHELL
(dolist (mode   '(org-mode-hook
                 term-mode-hook
                 eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

; KEYBINDINGS
; Lets escape quit prompt
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

; FONT
; Default font is Iosevka
(set-face-attribute 'default nil :font "Iosevka Nerd Font Mono" :height 130)

; Set the fixed pitch face
(set-face-attribute 'fixed-pitch nil :font "Iosevka Nerd Font Mono" :height 150)

; Set the variable pitch face
(set-face-attribute 'variable-pitch nil :font "Iosevka Nerd Font Mono" :height 175 :weight 'regular)

; PACKAGES
; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

; For the non-linux
(unless (package-installed-p 'use-package)
            (package-install 'use-package))


(require 'use-package)
(setq use-package-always-ensure t)

; For a better M-x
(use-package ivy
    :diminish
    :bind (("C-s" . swiper)
        :map ivy-minibuffer-map
        ("TAB" . ivy-alt-done)	
        ("C-l" . ivy-alt-done)
        ("C-j" . ivy-next-line)
        ("C-k" . ivy-previous-line)
        :map ivy-switch-buffer-map
        ("C-k" . ivy-previous-line)
        ("C-l" . ivy-done)
        ("C-d" . ivy-switch-buffer-kill)
        :map ivy-reverse-i-search-map
        ("C-k" . ivy-previous-line)
        ("C-d" . ivy-reverse-i-search-kill))
    :config
    (ivy-mode 0))

; M-x decription
(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-ibuffer)
         ("C-x C-f" . counsel-find-file)
         ("C-M-l" . counsel-imenu)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :config
  (setq ivy-initial-inputs-alist nil)) ;; Don't start searches with ^
; For Doom-modeline icons
(use-package all-the-icons)

; Bar at the bottom
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

; Doom themes
(use-package doom-themes
    ; Nord theme
    :init (load-theme 'doom-nord t))

; Parenthesis reminder
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

; Buffer in side to  see keystrokes
(use-package which-key
    :init (which-key-mode)
    :diminish which-key-mode
    :config
    (setq which-key-idle-delay 0))

;; Helpful Help
(use-package helpful
  :ensure t
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

; EVIL
(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

; General-el key binding package
(use-package general
    :config
    (general-create-definer tea/leader-keys
        :keymaps '(normal insert visual emacs)
        :prefix "SPC"
        :global-prefix "C-SPC"))

; Make keybinding with the CTRL-SPACE prefic
    (tea/leader-keys
        't'     '(:ignore t :which-key "toggles")
        'tt'    '(counsel-load-theme :which-key "choose theme"))

(general-define-key
  "C-M-j" 'counsel-switch-buffer)

;; Hydra
(defhydra hydra-text-scale (:timeout 5)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

(tea/leader-keys
  "ts" '(hydra-text-scale/body :which-key "scale text"))

;; Projectile
(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  ;; NOTE: Set this to the folder where you keep your Git repos!
  (when (file-directory-p "~/Projects/Code")
    (setq projectile-project-search-path '("~/Projects/Code")))
  (setq projectile-switch-project-action #'projectile-dired))

; Uncomment this for verbose package load up
;(setq use-package-verbose t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("37768a79b479684b0756dec7c0fc7652082910c37d8863c35b702db3f16000f8" default))
 '(package-selected-packages '(nord-theme use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
